package com.frans.lifegame;

/**
 * @author franswoo
 *
 */
public class Cell {
    private LifeStatus status;			//当前状态
    private LifeStatus nextStatus;		//下一个状态
    private int x;						//细胞横坐标
    private int y;						//细胞纵坐标
    
    public Cell(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * 使细胞进化到下一个状态
     * @return 状态是否改变过
     */
    public boolean evolve() {
        if (status == nextStatus)
            return false;		//状态未改变过
        status = nextStatus;	//状态变迁
        return true;
    }
    
    //获取下一个状态变化值
    public LifeStatus getNextStatus() {
        return nextStatus;
    }

    public void setNextStatus(LifeStatus nextStatus) {
        this.nextStatus = nextStatus;
    }

    public void setStatus(LifeStatus status) {
        this.status = status;
    }
    
    public LifeStatus getStatus() {
        return status;
    }


    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
    public enum LifeStatus{
        SURVIVAL,
        DEATH,
    }
}

